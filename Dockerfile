# Completez le Dockerfile afin de faire fonctionner le serveur Rails
FROM ruby:2.5.8

# Completez ici...
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY . /myapp
EXPOSE 3000

# Conservez les lignes ci-dessous
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# N'oubliez pas la commande pour démarrer le serveur
CMD ["rails", "server", "-b", "0.0.0.0"]